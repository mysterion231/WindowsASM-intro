; msgbox.asm
	
.386
.model flat, stdcall
option casemap:none

      include \masm32\include\windows.inc
      include \masm32\include\user32.inc
      include \masm32\include\kernel32.inc

      includelib \masm32\lib\user32.lib
      includelib \masm32\lib\kernel32.lib

.data
	msgbox1path	db	"msgbox1.exe",0
	wintitle db "INJECTED", 0
	winmessage db "injection succeeded", 0
	
.code

start:
    push NULL
    push FILE_ATTRIBUTE_NORMAL
    push OPEN_EXISTING
    push NULL
    push NULL
    push GENERIC_READ+GENERIC_WRITE
    push offset msgbox1path
    call CreateFile    
    cmp eax, INVALID_HANDLE_VALUE
    je _end

    push NULL
    push NULL
    push NULL
    push PAGE_READWRITE
    push NULL
    push eax
    call CreateFileMapping

    push NULL
    push NULL
    push NULL
    push FILE_MAP_ALL_ACCESS
    push eax
    call MapViewOfFile

	
	; copy my title to msgbox1 .data section
	cld
	lea esi, [offset wintitle]
	lea edi, [eax + 820h]
	mov ecx, 8
	rep movsb
	
	; copy my message to msgbox1 .data section
	cld
	lea esi, [offset winmessage]
	lea edi, [eax + 830h]
	mov ecx, 19
	rep movsb

	; change jmp offset (jmp to the injected code)
    mov BYTE PTR [eax + 401h], 26h
	
	; arg of the injected message box
	; first push 0
    mov BYTE PTR [eax + 428h], 6Ah
    mov BYTE PTR [eax + 429h], 0h
    
	; first string (title)
    mov BYTE PTR [eax + 42Ah], 68h
	mov DWORD PTR [eax + 42Bh], 00403020h
	
	; second string (message)
	mov BYTE PTR [eax + 42Fh], 68h
	mov DWORD PTR [eax + 430h], 00403030h
	
    ; second push 0
    mov BYTE PTR [eax + 434h], 6Ah
    mov BYTE PTR [eax + 435h], 0H
    
	; call to message box
    mov BYTE PTR [eax + 436h], 0E8h
    mov DWORD PTR [eax + 437h], 0FFFFFFE1h
 
	; jmp to message box of msgbox1
	mov BYTE PTR [eax + 43Bh], 0EBh
    mov BYTE PTR [eax + 43Ch], 0C5h
 	
    push eax
    call UnmapViewOfFile

_end:	
	push	0
	call	ExitProcess

end	start